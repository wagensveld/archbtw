# pets

## Installing Arch

The first pet is installing arch itself.

1. [Create a bootable USB](https://wiki.archlinux.org/title/USB_flash_installation_medium): `dd bs=4M if=path/to/archlinux-version-x86_64.iso of=/dev/disk/by-id/usb-My_flash_drive conv=fsync oflag=direct status=progress`
2. Boot into the USB.
3. Ensure you have an Internet connection
4. Sync package db: `pacman -Syy`
5. Install archinstall: `pacman -S archinstall`
6. Run archinstall: `archinstall`

There are a few settings to set

| Option                  | Value |
| ----------------------- | - |
| Language                | English |
| Mirror Region           | Australia | 
| Harddrives              | your-harddrive |
| Disk layout             | file system: btrfs | 
| Encryption Password     | hunter1 |
| Bootloader              | grub-install |
| Swap                    | True |
| Hostname                | something-cool |
| Root password           | hunter2 |
| Audio                   | pipewire |
| Kernels                 | linux |
| Additional packages     | kitty plasma-meta plasma-wayland-session networkmanager|
| Additional repositories | multilib |

7. Install

Afterwards boot into the arch, ensure the following are enabled:
- `systemctl enable sddm`
- `systemctl enable NetworkManager.service`

You should now be able to log in.

## Installing Git (and SSH)

Run `pacman -S git`, at the same time it's probably worth setting up ssh, `pacman -S openssh`, followed by `ssh-keygen -t ed25519 -C "your_email@example.com"`.

### Paru
Install [paru](https://github.com/Morganamilo/paru)

```
sudo pacman -S --needed base-devel
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```

## Installing Ansible and Running Cattle
Run `pacman -S ansible`. Afterwards clone this repo, change directory to `cattle`.

Run: `ansible-playbook main.yml --ask-become-pass`

## Other Stuff

Afterwards other things to do are:

### Copy files over

In the `files` directory are some config files which do not appreciate being symlinked.

### Set up KeePassXC

1. Open KeePassXC
2. Tools -> Settings -> General. Set "Backup database file before saving". Set "Use alternative saving method"
3. Tools -> Settings -> Browser Integration. Enable browser integration.

### Set up Timeshift

Timeshift is installed as a part of the ansible playbook but there's additional setup. Open timeshift for that. Afterwards you can install the following:


`paru -S timeshift-autosnap` this automatically creates a snapshot when installing packages.

`pacman -S grub-btrfs` this lets you boot from a snapshot via grub.

To use grub-btrfs you then need to run the following 

`sudo EDITOR=nvim systemctl edit --full grub-btrfsd` 

Change `ExecStart=/usr/bin/grub-btrfsd --syslog /.snapshots` to `ExecStart=/usr/bin/grub-btrfsd --syslog --timeshift-auto`

`sudo grub-mkconfig -o /boot/grub/grub.cfg`

`sudo systemctl enable --now grub-btrfsd`