# KDE

In `~/.config/kdeglobals` ensure the following lines are included.

```
[General]
BrowserApplication=firefox.desktop
TerminalApplication=kitty
TerminalService=kitty.desktop
accentColorFromWallpaper=true

[Icons]
Theme=Papirus-Dark

[KDE]
SingleClick=false
```