# archbtw

A redo of sorts of my [config](https://gitlab.com/wagensveld/config). Streamlined to be a bit more practical.

## Cattle and Pets

I'm not really planning on having my desktop spun up and destroyed on a regular basis; there are quite a few things I only need to do once, such as `timedatectl set-ntp true`. Or more finicky things like setting up passkeys for disk encryption. 

Information for that sort of thing lives in the `pets` directory.

Likewise there's the `cattle` directory for more routine tasks.

I recommend tackling the pets before the cattle.
