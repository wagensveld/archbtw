# archbtw

First thing's first before running the cattle we need ansible installed. `sudo pacman -S ansible`

Afterwards it can be run like so `ansible-playbook main.yml --ask-become-pass`. This will run everything.

Each role has a corresponding tag with its own name. You can run `ansible-playbook main.yml --ask-become-pass --tags "bluetooth firefox"` for example.