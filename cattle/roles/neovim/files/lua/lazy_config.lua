-- Bootstrap lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

vim.g.mapleader = " " -- Make sure to set `mapleader` before lazy so your mappings are correct

require("lazy").setup({
  {
    -- Gruvbox theme
    "sainnhe/gruvbox-material",
      config = function()
        vim.o.background = "dark"
        vim.o.termguicolors = true
        vim.g.gruvbox_material_background = 'hard'
        vim.g.gruvbox_material_better_performance = 1
        vim.cmd [[colorscheme gruvbox-material"]]
      end
  },
  {
    -- Dim paragraphs not being worked on
    "folke/twilight.nvim"
  },
  {
    -- Distraction free coding
    "folke/zen-mode.nvim",
    opts = {
      window = {
        options = {
          number = false,
          list = false,
          relativenumber = false,
          cursorcolumn = false
        }
      }
    }
  },
  {
    -- Autocomplete brackets and the like
    'windwp/nvim-autopairs',
    event = "InsertEnter",
    opts = {} -- this is equalent to setup({}) function
  },
  {
    -- Preview lines without jumping to the line
    "nacro90/numb.nvim",
    config = function()
      require("numb").setup()
    end
  },
  {
    -- Colorise hex codes e.g. #008080
    "NvChad/nvim-colorizer.lua",
    config = function()
      require("colorizer").setup()
    end
  },
  {
  -- Show git info
  "lewis6991/gitsigns.nvim",
  config = function()
    require("gitsigns").setup()
  end
  },
  -- Plaintext notes
  {
  "mickael-menu/zk-nvim",
  config = function()
    require("zk").setup({
      -- See Setup section below
    })
  end
}
})

